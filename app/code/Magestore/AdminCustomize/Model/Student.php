<?php

namespace Magestore\AdminCustomize\Model;

use Magestore\AdminCustomize\Model\ResourceModel\Student as StudentResourceModel;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
class Student extends  AbstractModel implements  IdentityInterface
{
    const CACHE_TAG = 'student_grid';
    public function _construct()
    {
        $this->_init(StudentResourceModel::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
