<?php

namespace Magestore\AdminCustomize\Model\ResourceModel\Student;


use Magestore\AdminCustomize\Model\Student as StudentModel;
use Magestore\AdminCustomize\Model\ResourceModel\Student as StudentResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
 protected function _construct()
 {
     $this->_init(StudentModel::class, StudentResourceModel::class);
 }
}
