<?php

namespace Magestore\FAQ\Model\ResourceModel\Detail;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magestore\FAQ\Model\ResourceModel\Detail as DetailResourceModel;
use Magestore\FAQ\Model\Detail as DetailModel;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(DetailModel::class, DetailResourceModel::class);
        $this->_map['fields']['like'] = 'main_table.`like`';
        $this->addFilterToMap('like', 'main_table.like');

        $this->_map['fields']['dislike'] = 'main_table.`dislike`';
        $this->addFilterToMap('dislike', 'main_table.dislike');
    }
}
