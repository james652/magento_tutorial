<?php

namespace Magestore\FAQ\Model\Source;

use Magestore\FAQ\Model\ResourceModel\Category\CollectionFactory;

class Category implements \Magento\Framework\Option\ArrayInterface
{
    protected $collectionFactory;

    public function __construct(  CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $options[] = ['label' => '-- Please Select --', 'value' => ''];
        $categoryCollection = $this->collectionFactory->create();
        $categoryCollection->addFieldToSelect('*');
        foreach ($categoryCollection as $category) {
            $options[] = [
                'label' => $category['name'],
                'value' => $category['category_id'],
            ];
        }

        return $options;
    }
}
