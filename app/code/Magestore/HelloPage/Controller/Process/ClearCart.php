<?php

namespace Magestore\HelloPage\Controller\Process;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class ClearCart extends Action
{
    protected  $_modelCart;
    protected $checkoutSession;
    protected $resultRedirect;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        CheckoutSession $checkoutSession,
        Cart $modelCart)
    {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_modelCart = $modelCart;
        parent::__construct($context);

    }

    public function execute()
    {
        $cart = $this->_modelCart;
        $quoteItems = $this->checkoutSession->getQuote()->getItemsCollection();
        foreach ($quoteItems as $item) {
            $cart->removeItem($item->getId())->save();
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('checkout/cart');
        return $resultRedirect;
    }
}
