<?php

namespace Magestore\HelloWorld\Controller\Film;
use Magento\Framework\App\Action\Context;

class CategoryList extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected  $_categoryFactory;

    function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magestore\HelloWorld\Model\CategoryFactory $categoryFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $category = $this->_categoryFactory->create();
        $collection = $category->getCollection();

        $collection->getSelect()
            ->join(array('film_category' => 'zero_training_four_film_category'),
                'main_table.category_id = film_category.category_id',
                array()
            )
            ->join(array('film_actor' => 'zero_training_four_film_actor'),
                'film_category.film_id = film_actor.film_id',
                array()
            )
            ->group('film_category.category_id')
            ->order(' count(*) desc')
            ->limit(5);

        foreach ($collection as $item) {
            echo "<pre>";
            if (!str_contains($item->getName(), 'highest')) {
                $item->setName('highest ' . $item->getName());
                $item->save();
            }
            print_r($item->getData());
            echo "</pre>";
        }
        exit();
        return $this->_pageFactory->create();
    }
}

