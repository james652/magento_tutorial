<?php

namespace Magestore\HelloWorld\Controller\Film;
use Magento\Framework\App\Action\Context;

class ShowList extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected  $_locationFactory;

    function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magestore\HelloWorld\Model\LocationFactory $locationFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_locationFactory = $locationFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $location = $this->_locationFactory->create();
        $collection = $location->getCollection();
        $collection->getSelect()
            ->join(array('film_actor' => 'zero_training_four_film_actor'),
                'main_table.film_id = film_actor.film_id',
                array()
            )
            ->group('film_actor.film_id')
            ->having('count(*) > 5');
        foreach ($collection as $item) {
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        }
        exit();
        return $this->_pageFactory->create();
    }
}

