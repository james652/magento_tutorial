<?php

namespace Magestore\AddPlugin\Plugin;

use Magento\Checkout\Block\Cart\Item\Renderer;

class ChangeImageCheckout
{
    public function afterGetImage(Renderer $subject, $result, $product, $imageId, $attributes = [])
    {
        $result->setImageUrl("https://magestore-service.atlassian.net/s/azc3hx/b/8/3e5bf54407d016d63f9c1cd2bc89e653/_/jira-logo-scaled.png");
        return $result;
    }
}
