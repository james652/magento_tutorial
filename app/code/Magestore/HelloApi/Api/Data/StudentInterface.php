<?php
namespace Magestore\HelloApi\Api\Data;

interface StudentInterface{
    const NAME = 'name';
    const AGE = 'age';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get student id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set student id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get age
     *
     * @return int|null
     */
    public function getAge();

    /**
     * Set age
     *
     * @param int $age
     * @return $this
     */
    public function setAge($age);

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);







}
