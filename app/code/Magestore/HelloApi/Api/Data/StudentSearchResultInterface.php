<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\HelloApi\Api\Data;

/**
 * Interface for student search results.
 * @api
 * @since 100.0.2
 */
interface StudentSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get student list.
     *
     * @return \Magestore\HelloApi\Api\Data\StudentInterface[]
     */
    public function getItems();

    /**
     * Set student list.
     *
     * @param \Magestore\HelloApi\Api\Data\StudentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

