<?php
namespace Magestore\HelloApi\Model\ResourceModel\Student;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected function _construct()
    {
        $this->_init('Magestore\HelloApi\Model\Student', 'Magestore\HelloApi\Model\ResourceModel\Student');
    }
}
