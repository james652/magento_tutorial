<?php
namespace Magestore\HelloApi\Model\ResourceModel;

use Magestore\HelloApi\Api\Data\StudentSearchResultInterface;
use Magestore\HelloApi\Api\Data\StudentSearchResultInterfaceFactory;
use Magestore\HelloApi\Api\Data\StudentInterface;
use Magestore\HelloApi\Api\Data\StudentInterfaceFactory;
use Magestore\HelloApi\Api\StudentRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magestore\HelloApi\Model\StudentFactory;
use Magestore\HelloApi\Model\ResourceModel\Student as ResourceStudent;
use Magestore\HelloApi\Model\ResourceModel\Student\CollectionFactory as StudentCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;


class StudentRepository implements StudentRepositoryInterface{

    private $collectionProcessor;
    private $resource;
    private $collectionFactory;
    private $studentFactory;
    private $studentInterfaceFactory;
    private $searchResultsFactory;

    public function __construct(

        ResourceStudent                    $resource,
        StudentFactory                       $studentFactory,
        StudentInterfaceFactory              $studentInterfaceFactory,
        StudentCollectionFactory             $collectionFactory,
        StudentSearchResultInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface      $collectionProcessor
    )
    {
        $this->resource = $resource;
        $this->studentFactory = $studentFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->studentInterfaceFactory = $studentInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StudentSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        //$searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;

    }

    /**
     * @param int $studentID
     * @return StudentInterface
     * @throws NoSuchEntityException
     */
    public function getByID(int $studentID) : StudentInterface

    {

    $student = $this->studentFactory->create();
    $this->resource->load($student, $studentID);
    if(!$student->getId())
        throw new NoSuchEntityException(__('The student with the "%1" ID doesn\'t exist.', $studentID));
    return $student;
    }

    /**
     * @param int $studentID
     * @return bool
     * @throws NoSuchEntityException
     */
    public function deleteByID(int $studentID):bool
    {
        return $this->delete($this->getByID($studentID));
    }

    /**
     * @param StudentInterface $student
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(StudentInterface $student):bool
    {
        try {
            $this->resource->delete($student);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the student: %1', $exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @param StudentInterface $student
     * @return StudentInterface
     * @throws CouldNotSaveException
     */
    public function save(StudentInterface $student): StudentInterface
    {
        try {
            $this->resource->save($student);
        } catch (LocalizedException $exception) {
            throw new CouldNotSaveException(
                __('Could not save the student: %1', $exception->getMessage()),
                $exception
            );
        } catch (\Throwable $exception) {
            throw new CouldNotSaveException(
                __('Could not save the student: %1', __('Something went wrong while saving the post.')),
                $exception
            );
        }
        return $student;
    }
}
