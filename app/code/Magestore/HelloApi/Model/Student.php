<?php

namespace Magestore\HelloApi\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magestore\HelloApi\Api\Data\StudentInterface;
use Magestore\HelloApi\Model\ResourceModel\Student as ResourceModel;

class Student extends AbstractModel implements IdentityInterface, StudentInterface
{


    const CACHE_TAG = 'student_db';

    protected function _construct()
    {
        $this->_init('Magestore\HelloApi\Model\ResourceModel\Student');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getName()
    {
        return $this->_getData(SELF::NAME);
    }

    public function setName($name)
    {
        return $this->setData(SELF::NAME, $name);

    }


    public function getAge()
    {
        return $this->_getData(SELF::AGE);
    }

    public function setAge($age)
    {
        return $this->setData(SELF::AGE, $age);
    }

    public function getCreatedAt()
    {
        return $this->_getData(SELF::CREATED_AT);

    }

    public function setCreatedAt($createdAt)
    {
        return $this->setData(SELF::CREATED_AT, $createdAt);

    }

    public function getUpdatedAt()
    {
        return $this->_getData(SELF::UPDATED_AT);
    }

    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(SELF::UPDATED_AT, $updatedAt);
    }
}
