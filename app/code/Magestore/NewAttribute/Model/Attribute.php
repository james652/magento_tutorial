<?php

namespace Magestore\NewAttribute\Model;

use Magento\Framework\Model\AbstractModel;
use Magestore\NewAttribute\Model\ResourceModel\Attribute as ResourceModel;

class Attribute extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magestore\NewAttribute\Model\ResourceModel\Attribute');
    }
}
