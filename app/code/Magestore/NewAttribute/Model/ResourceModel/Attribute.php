<?php

namespace Magestore\NewAttribute\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Attribute extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('catalog_product_entity', 'entity_id');
    }
}

