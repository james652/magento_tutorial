<?php

namespace Magestore\NewAttribute\Model\ResourceModel\Attribute;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magestore\HelloWorld\Model\Location as Model;
use Magestore\HelloWorld\Model\ResourceModel\Location as ResourceModel;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magestore\NewAttribute\Model\Attribute', 'Magestore\NewAttribute\Model\ResourceModel\Attribute');
    }
}

